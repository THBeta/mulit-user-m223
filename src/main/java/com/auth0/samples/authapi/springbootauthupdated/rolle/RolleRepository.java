package com.auth0.samples.authapi.springbootauthupdated.rolle;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * @author: Thomas Hilberink
 * @date: 10.07.2020
 * 
 */

public interface RolleRepository extends JpaRepository<Rolle, Long> {
}