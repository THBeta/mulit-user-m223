package com.auth0.samples.authapi.springbootauthupdated.event;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * @author: Thomas Hilberink
 * @date: 10.07.2020
 * 
 */

public interface EventRepository extends JpaRepository<Event, Long> {
}