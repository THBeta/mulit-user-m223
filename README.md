**Was kann diese Programm**

Mit dieses Programm kann man sich einloggen und User erstellen. Die User können Tasks erstellen.
Zudem gibt es mehrere Rollen und die User können beliebig viele haben. 
Admins können Events veranstalten und diese jeweils an Rollen zuteilen. 
Das programm lauft mit der Hilfe von eine In-memory database. 


**Wie man das Programm starten kann.**

- Herunterladen von Git
- Als gradle project importieren(es kann sein, dass sie noch einige Sachen zu gradle herunterladen müssen).
- Progamm starten und mit dem Postman kann man überprüfen ob die Funktionen funktionieren.
- In der Klasse SpringbootAuthUpdatedApplication kann man default Daten hineinfügen.
- Mit der Klasse SpringbootAuthUpdatedApplication kann man das Programm starten. 